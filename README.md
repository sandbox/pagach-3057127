This module creates a simple kanban board with the ability to add lists and card to board.
Module is built upon jKanban (https://github.com/riktar/jkanban) but extended with additional features.

Dependencies:

- jKanban (https://github.com/riktar/jkanban)

Installation:

- Download jKanban to libraries folder
- Make sure jKanban css is accessible on /libraries/jkanban/dist/jkanban.min.css
- Make sure jKanban js is accessible on /libraries/jkanban/dist/jkanban.min.js
- Install module as usual
- Go to /admin/content/kanban_board and create your first board
