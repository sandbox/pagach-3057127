<?php

namespace Drupal\jkanban\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Board entities.
 *
 * @ingroup jkanban
 */
class BoardEntityDeleteForm extends ContentEntityDeleteForm {


}
