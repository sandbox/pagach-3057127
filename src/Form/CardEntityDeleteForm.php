<?php

namespace Drupal\jkanban\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Card entities.
 *
 * @ingroup jkanban
 */
class CardEntityDeleteForm extends ContentEntityDeleteForm {


}
