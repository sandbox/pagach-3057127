<?php

namespace Drupal\jkanban\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting List entities.
 *
 * @ingroup jkanban
 */
class ListEntityDeleteForm extends ContentEntityDeleteForm {


}
