<?php

namespace Drupal\jkanban\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining List entities.
 *
 * @ingroup jkanban
 */
interface ListEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the List name.
   *
   * @return string
   *   Name of the List.
   */
  public function getName();

  /**
   * Sets the List name.
   *
   * @param string $name
   *   The List name.
   *
   * @return \Drupal\jkanban\Entity\ListEntityInterface
   *   The called List entity.
   */
  public function setName($name);

  /**
   * Gets the List creation timestamp.
   *
   * @return int
   *   Creation timestamp of the List.
   */
  public function getCreatedTime();

  /**
   * Sets the List creation timestamp.
   *
   * @param int $timestamp
   *   The List creation timestamp.
   *
   * @return \Drupal\jkanban\Entity\ListEntityInterface
   *   The called List entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the List published status indicator.
   *
   * Unpublished List are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the List is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a List.
   *
   * @param bool $published
   *   TRUE to set this List to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\jkanban\Entity\ListEntityInterface
   *   The called List entity.
   */
  public function setPublished($published);

}
