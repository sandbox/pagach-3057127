<?php

namespace Drupal\jkanban\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Board entities.
 *
 * @ingroup jkanban
 */
interface BoardEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Board name.
   *
   * @return string
   *   Name of the Board.
   */
  public function getName();

  /**
   * Sets the Board name.
   *
   * @param string $name
   *   The Board name.
   *
   * @return \Drupal\jkanban\Entity\BoardEntityInterface
   *   The called Board entity.
   */
  public function setName($name);

  /**
   * Gets the Board creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Board.
   */
  public function getCreatedTime();

  /**
   * Sets the Board creation timestamp.
   *
   * @param int $timestamp
   *   The Board creation timestamp.
   *
   * @return \Drupal\jkanban\Entity\BoardEntityInterface
   *   The called Board entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Board published status indicator.
   *
   * Unpublished Board are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Board is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Board.
   *
   * @param bool $published
   *   TRUE to set this Board to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\jkanban\Entity\BoardEntityInterface
   *   The called Board entity.
   */
  public function setPublished($published);

}
