<?php

namespace Drupal\jkanban;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the List entity.
 *
 * @see \Drupal\jkanban\Entity\ListEntity.
 */
class ListEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\jkanban\Entity\ListEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished list entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published list entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit list entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete list entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add list entities');
  }

}
