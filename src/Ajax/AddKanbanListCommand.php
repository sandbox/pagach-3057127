<?php

namespace Drupal\jkanban\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class AddKanbanListCommand.
 */
class AddKanbanListCommand implements CommandInterface {

  /**
   * Render custom ajax command.
   *
   * @return ajax
   *   Command function.
   */
  public function render() {
    return [
      'command' => 'addKanbanList',
      'message' => 'My Awesome Message',
    ];
  }

}
