<?php

namespace Drupal\jkanban\Controller;

use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;

/**
 * Class AddKanbanListCallbackController.
 */
class AddKanbanListCallbackController extends ControllerBase
{

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $entityFormBuilder;

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ModalFormExampleController constructor.
   *
   * @param EntityFormBuilderInterface $formBuilder
   *   The form builder.
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityFormBuilderInterface $entityFormBuilder, EntityTypeManagerInterface $entityTypeManager)
  {
    $this->entityFormBuilder = $entityFormBuilder;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity.form_builder'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Add kanban list callback method.
   *
   * @param $board_id
   *   Board id
   * @return AjaxResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function addKanbanListCallback($board_id)
  {
    $response = new AjaxResponse();
    $kanban_list = $this->entityTypeManager()->getStorage('kanban_list_entity')->create();
    $kanban_list_form = $this->entityFormBuilder()->getForm($kanban_list);
    $response->addCommand(new OpenModalDialogCommand($this->t('Create new list'), $kanban_list_form, ['width' => '800']));

    return $response;
  }

}
